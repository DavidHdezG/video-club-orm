module.exports = (sequelize, DataTypes) => {
  const Genre = sequelize.define("genres", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    description: DataTypes.STRING,
    status: DataTypes.BOOLEAN,
  });
  return Genre;
};
