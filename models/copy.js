module.exports = (sequelize, DataTypes) => {
    const Copy = sequelize.define("copies", {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      number: DataTypes.INTEGER,
        movieId: DataTypes.INTEGER,
        format: DataTypes.STRING,
        status: DataTypes.BOOLEAN,
    });

    return Copy;
  };
  