module.exports = (sequelize, DataTypes) => {
    const Director = sequelize.define("directors", {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: DataTypes.STRING,
      lastName: DataTypes.STRING,
    });

    return Director;
  };
  