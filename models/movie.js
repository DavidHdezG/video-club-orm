module.exports = (sequelize, DataTypes) => {
    const Movie = sequelize.define("movies", {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      title: DataTypes.STRING,
    });

    return Movie;
  };
  