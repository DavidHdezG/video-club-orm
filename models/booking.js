module.exports = (sequelize, DataTypes) => {
    const Booking = sequelize.define("bookings", {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      date: DataTypes.DATE,
      memberId: DataTypes.INTEGER,
      copyId: DataTypes.INTEGER,
    });

    return Booking;
  };
  