FROM node
MAINTAINER David Hernandez
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start
