const Sequelize = require("sequelize");
const genreModel = require("./models/genre");
const movieModel = require("./models/movie");
const directorModel = require("./models/director");
const bookingModel = require("./models/booking");
const CopyModel = require("./models/Copy");
const memberModel = require("./models/member");
const actorModel = require("./models/actor");
const movieActorModel = require("./models/movieActor");

const sequelize = new Sequelize("video_club", "Tarea", "tarea", {
  host: "localhost",
  dialect: "mysql",
});

const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Director = directorModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const Copy = CopyModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const MovieActor = movieActorModel(sequelize, Sequelize);
// un genero tiene muchas peliculas
Genre.hasMany(Movie, { as: "movies" });
// una pelicula pertenece a un genero
Movie.belongsTo(Genre, { as: "genres" });

Director.hasMany(Movie, { as: "movies" });
Movie.belongsTo(Director, { as: "directors" });

Movie.hasMany(Copy, { as: "Copys" });
Copy.belongsTo(Movie, { as: "movies" });

Copy.hasMany(Booking, { as: "bookings" });
Booking.belongsTo(Copy, { as: "copies" });

Member.hasMany(Booking, { as: "bookings" });
Booking.belongsTo(Member, { as: "members" });

MovieActor.belongsTo(Movie, { foreignKey: "movieId" });
MovieActor.belongsTo(Actor, { foreignKey: "actorId" });

Movie.belongsToMany(Actor, {
  foreignKey: "actorId",
  as: "actors",
  through: "moviesActors",
});

Actor.belongsToMany(Movie, {
  foreignKey: "movieId",
  as: "movies",
  through: "moviesActors",
});
sequelize
  .sync({
    force: false,
  })
  .then(() => {
    console.log("Tablas sincronizadas");
  });

module.exports = {
  Genre,
  Movie,
  Member,
  Booking,
  Copy,
  Director,
  Actor,
  MovieActor,
};
