const express = require('express');
const createError = require('http-errors');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const genreRouter = require('./routes/genres');
const movieRouter = require('./routes/movies');
const actorRouter = require('./routes/actors');
const directorRouter = require('./routes/directors');
const copyRouter = require('./routes/copies');
const memberRouter = require('./routes/members');
const bookingRouter = require('./routes/bookings');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/genres', genreRouter);
app.use('/movies', movieRouter);
app.use('/actors', actorRouter);
app.use('/directors', directorRouter);
app.use('/copies', copyRouter);
app.use('/members', memberRouter);
app.use('/bookings', bookingRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
