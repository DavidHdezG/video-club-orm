const express = require("express");
const { Booking } = require("../db");

function list(req, res, next) {
  Booking.findAll({ include: ["members", "Copys"] })
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}

function index(req, res, next) {
  const id = req.params.id;
  Booking.findByPk(id)
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}
function create(req, res, next) {
  const date = req.body.date;
  const Booking_id = parseInt(req.body.Booking_id);
  const copy_id = parseInt(req.body.copy_id);
  let Booking = new Object({
    date: date,
    BookingId: Booking_id,
    copyId: copy_id,
  });
  Booking.create(Booking)
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}
function replace(req, res, next) {
  const id = req.params.id;
  Booking.findByPk(id)
    .then((object) => {
      const date = req.body.date ? req.body.date : "";
      const Booking_id = parseInt(req.body.Booking_id)
        ? parseInt(req.body.Booking_id)
        : null;
      const copy_id = parseInt(req.body.copy_id)
        ? parseInt(req.body.copy_id)
        : null;
      object
        .update({ date: date, BookingId: Booking_id, copyId: copy_id })
        .then((obj) => res.json(obj))
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));
  res.send("respond with replace");
}
function update(req, res, next) {
  const id = req.params.id;
  Booking.findByPk(id)
    .then((object) => {
      const date = req.body.date ? req.body.date : object.date;
      const Booking_id = parseInt(req.body.Booking_id)
        ? parseInt(req.body.Booking_id)
        : object.BookingId;
      const copy_id = parseInt(req.body.copy_id)
        ? parseInt(req.body.copy_id)
        : object.copyId;
      object
        .update({ date: date, BookingId: Booking_id, copyId: copy_id })
        .then((obj) => res.json(obj))
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));
}
function destroy(req, res, next) {
  const id = req.params.id;
  Booking.destroy({
    where: {
      id: id,
    },
  })
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}

module.exports = { list, index, create, replace, update, destroy };
