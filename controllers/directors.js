const express = require("express");
const { Movie } = require("../db");

function list(req, res, next) {
  Movie.findAll({ include: ["movies"] })
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}

function index(req, res, next) {
  const id = req.params.id;
  Movie.findByPk(id)
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}
function create(req, res, next) {
  const name = req.body.name;
  const last_name = req.body.last_name;
  let movie = new Object({
    name: name,
    last_name: last_name,
  });

  Movie.create(movie)
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}
function replace(req, res, next) {
  const id = req.params.id;
  Movie.findByPk(id)
    .then((object) => {
      const name = req.body.name ? req.body.name : "";
      const last_name = req.body.last_name ? req.body.last_name : "";
      object
        .update({ name: name, last_name: last_name })
        .then((obj) => res.json(obj))
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));
}
function update(req, res, next) {
  const id = req.params.id;
  Movie.findByPk(id)
    .then((object) => {
      const name = req.body.name ? req.body.name : object.name;
      const last_name = req.body.last_name
        ? req.body.last_name
        : object.last_name;
      object
        .update({ name: name, last_name: last_name })
        .then((genre) => res.json(genre))
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));
}
function destroy(req, res, next) {
  const id = req.params.id;
  Movie.destroy({
    where: {
      id: id,
    },
  })
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}

module.exports = { list, index, create, replace, update, destroy };
