const express = require("express");
const { Movie } = require("../db");

function list(req, res, next) {
    Movie.findAll({include:['genres','actors','directors']})
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}

function index(req, res, next) {
  const id = req.params.id;
  Movie.findByPk(id)
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}
function create(req, res, next) {
  const title = req.body.title;
  const genre_id= parseInt(req.body.genre_id);
  const director_id= parseInt(req.body.director_id);
  let movie = new Object({
    title: title,
    genreId: genre_id,
    directorId: director_id
  });
  Movie.create(movie)
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}
function replace(req, res, next) {
  const id = req.params.id;
  Movie.findByPk(id)
    .then((object) => {
      const title = req.body.title ? req.body.title : "";
      object
        .update({ title: title})
        .then((obj) => res.json(obj))
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));
}
function update(req, res, next) {
  const id = req.params.id;
  Movie.findByPk(id)
    .then((object) => {
        const title = req.body.title ? req.body.title : object.title;
      object
        .update({ title: title})
        .then((genre) => res.json(genre))
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));
}
function destroy(req, res, next) {
  const id = req.params.id;
  Movie.destroy({where:{
    id:id
  }}).then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}

module.exports = { list, index, create, replace, update, destroy };
 