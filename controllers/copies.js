const express = require("express");
const { Copy } = require("../db");

function list(req, res, next) {
  Copy.findAll({ include: ["movies"] })
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}

function index(req, res, next) {
  const id = req.params.id;
  Copy.findByPk(id)
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}
function create(req, res, next) {
  const number = req.body.number;
  const format = req.body.format;
  const member_id = req.body.member_id;
  const copy_id = req.body.copy_id;
  let Copy = new Object({
    number: number,
    format: format,
    member_id: member_id,
    copy_id: copy_id,
  });
  Copy.create(Copy)
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}
function replace(req, res, next) {
  const id = req.params.id;
  Copy.findByPk(id)
    .then((object) => {
        const number = req.body.number? req.body.number : "";
        const format = req.body.format? req.body.format : "";
        const movie_id = req.body.movie_id? req.body.movie_id : "";
        const status = req.body.status? req.body.status : null;
      object
        .update({ number: number, format: format, movie_id: movie_id, status: status })
        .then((obj) => res.json(obj))
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));
}
function update(req, res, next) {
  const id = req.params.id;
  Copy.findByPk(id)
    .then((object) => {
        const number = req.body.number? req.body.number : object.number;
        const format = req.body.format? req.body.format : object.format;
        const movie_id = req.body.movie_id? req.body.movie_id : object.movie_id;
        const status = req.body.status? req.body.status : object.status;
      object
        .update({ number: number, format: format, movie_id: movie_id, status: status })
        .then((obj) => res.json(obj))
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));
}
function destroy(req, res, next) {
  const id = req.params.id;
  Copy.destroy({
    where: {
      id: id,
    },
  })
    .then((obj) => res.json(obj))
    .catch((err) => res.send(err));
}

module.exports = { list, index, create, replace, update, destroy };
